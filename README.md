Installing NodeJS
```bash
$ sudo apt install nodejs npm
$ nodev -v && npm -v
```


Creating a React Application
```bash
$ npx create-react-app my-app
```


Running React Development Server
```bash
$ cd my-app && npm start
```
