import React, { useState, useEffect } from 'react';
import './App.css'
import UserCard from './components/UserCard'

const App = () => {
  const [users, setUsers] = useState([]);

  useEffect(() => {
    fetch("https://randomuser.me/api/?results=5")
    .then(response => response.json())
    .then(data => {
       setUsers(data.results)
     });
  }, []);
    
  return (
    <div>
      {
        users.map((user, idx) => {
          return (
            <UserCard key={idx}
              name={user.name.first}
              email={user.email}
              age={user.dob.age}
              avatarUrl={user.picture.large}
            />
          )
        })
      }
    </div>
  )
}

export default App;