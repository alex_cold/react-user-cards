import React, {useState} from 'react';

const UserCard = (props) => {
    // Creating a React-hook, using destruction assignment. Default value for the state is false.
    const [showAge, setShowAge] = useState(false)

    return (
        <div className="contact-card">
            <img src={props.avatarUrl} alt="profile-img"></img>
            <div className="user-details">
                <p>Name: {props.name}</p>
                <p>Email: {props.email}</p>
                <button onClick={() => setShowAge(!showAge)}>Show Age</button>

                {/* {showAge ? <p>Age: 1337</p> : null} */}
                {showAge && <p>Age: {props.age}</p>}
            </div>
      </div>
    )
}

export default UserCard;